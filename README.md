# json api 練習

一律使用 POST 

作用網址為

http://localhost/{action}

action 為四種動作：add, del, update, query

增：http://localhost/add

刪：http://localhost/del

改：http://localhost/update

查：http://localhost/query


### 資料結構
Schema: Demo

Table: USER

Column: ID, NAME, PHONE

(PK) ID：使用者的ID 

NAME：使用者姓名

PHONE：使用者手機

透過對四種動作的 api 網址提交 post 
json 電文，對 USER table 增刪改查


增  
上行電文範例
```
{
    "ID": "1",
    "NAME": "Peter",
    "PHONE": "0912345678"
}
```

下行電文範例
成功
```
{
	
	"Status": "Success"
}
```
失敗
```
{
	
	"Status": "FAILED"
}
```
刪
上行電文範例
第一種
```
{
    "ID": "1",
    "NAME": "Peter",
    "PHONE": "0912345678"
    
}
```
第二種
```
{
    "ID": "1"

}
```
下行電文範例
```
{
	
	"Status": "Success"
}
```
失敗
```
{
	
	"Status": "FAILED"
}

```
改
上行電文範例
第一種
```
{
    "ID": "1",
    "NAME": "Peter",
    "PHONE": "0912345678"
}
```
第二種
```
{
    "ID": "1",
    "PHONE": "0912345678"
}
```
下行電文範例
```
{
	
	"Status": "Success"
}
```
失敗
```
{
	
	"Status": "FAILED"
}
```
查
上行電文範例
第一種：
```
{
    "ID": "1"
}
```
第二種
```
{
    "NAME": "Peter"
}
```
第三種
```
{
    "NAME": ""
}
```
下行電文範例
成功
```
{
    "UserList": [
        {
            "ID": "1",
            "NAME": "Peter",
            "PHONE": "0912345678"
        },
        {
            "ID": "1",
            "NAME": "John",
            "PHONE": "0912345678"
        }
    ]
}
}
```
失敗
```
{
	
	"Status": "NO DATA"
}
```

實現技術：Springboot

DB: H2

時間：兩天

#### 非必要但可增加的功能：

對資料作檢核，比如某些動作ID不可為空，

輸入PHONE必須有十位否則報錯
