package com.example.service.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonCrudApplication.class, args);
	}

}
