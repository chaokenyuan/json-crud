package com.example.service.app.entity.shop;

import lombok.Data;

@Data
public class Member {

    private String account;

    private String name;

    public Member(String account,String name){
        this.account = account;
        this.name = name;
    }
}
